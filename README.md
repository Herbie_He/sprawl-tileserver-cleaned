![tileserver-gl](https://cloud.githubusercontent.com/assets/59284/18173467/fa3aa2ca-7069-11e6-86b1-0f1266befeb6.jpeg)

Instructions for serving tiles with Docker using the lastest Dockerfile from https://github.com/maptiler/tileserver-gl

# TileServer GL
[![GitHub Workflow Status](https://img.shields.io/github/actions/workflow/status/maptiler/tileserver-gl/pipeline.yml)](https://github.com/maptiler/tileserver-gl/actions/workflows/pipeline.yml)
[![Docker Hub](https://img.shields.io/badge/docker-hub-blue.svg)](https://hub.docker.com/r/maptiler/tileserver-gl/)

Vector and raster maps with GL styles. Server-side rendering by MapLibre GL Native. Map tile server for MapLibre GL JS, Android, iOS, Leaflet, OpenLayers, GIS via WMTS, etc.


## Getting Started with Docker
Make sure you have Node and Docker installed

clone this repository
```bash
git clone https://gitlab.com/cpbl/sprawl-tileserver.git
```

### Verify Docker permission
Add your current user to docker group (or ask your admin to do this)
```bash
sudo usermod -aG docker $USER
```
Switch session to docker group
```bash
newgrp - docker
```

### Build Docker Image
Run the following to build a Docker Image from Dockerfile and name the Docker Image as `$DOCKER_IMAGE_NAME`. The `--no-cache` option ensures the image is built from scratch (image built will not share the same image id with existing/deleted image).
```bash
docker build -f Dockerfile . --no-cache -t $DOCKER_IMAGE_NAME
```

Run the following to ensure the Docker Image is built sucessfully. You should see `$DOCKER_IMAGE_NAME` showing.
```bash
docker image ls
```
Before launching this docker image, ensure you have these two files in the **current directory** (i.e the local directory  where the docker image is built and launched)
- `final.mbtiles`: The vector tiles that you are serving. Note that you should name or rename your mbtiles files as `final.mbtiles` so the Docker image can locate it. Alternatively, you can also modify the `"data"`>`"v3"`>`"mbtiles"` attribute in the `config.json` to match the name of your vector tiles.

- `config.json`: The configuration file used by Docker Image to locate the mbtiles data. The `condfig.json` provided in this repo is sufficient and ready-to-use (i.e no modification is required). See the documentation for config files [here](https://tileserver.readthedocs.io/en/latest/config.html).


### Launch Docker image
Once you have the `final.mbtiles` and `config.json` files, run the following command to launch the docker image. Replace `$DOCKER_IMAGE_NAME` with actual Docker image name (use `docker image ls` to list all Docker Images). 

Replace `$PORT` with the port number that you want the tile server to run on (e.g 3010). 


```bash
docker run --rm -it -v $(pwd):/data -p $PORT:8080 $DOCKER_IMAGE_NAME
```

Check the status of the Tile Server by running the following

```bash
docker ps
```
### Check result
 If the Tile Server is running sucessfully, `visit https://$SERVER_IP:$PORT` to view the tileserver-gl interface. Click "Inspect" under the Data section to view the tiles.
 
 **Note**: The tiles for sprawlmap.org is served at port 3010 on sprawl server. However, we use https://tiles.sprawl.research.mcgill.ca/ as a proxy for the tile server running on port 3010. So visit https://tiles.sprawl.research.mcgill.ca to see the tileserver-gl interface for the tiles served for sprawlmap.org



## Documentation

You can read the full documentation of this project at https://maptiler-tileserver.readthedocs.io/.


